M4LIB = macroweb

VPATH = $(M4LIB)

export DEST = /home/groups/o/of/ofset/htdocs/assetml

export DATA=./

INSTALL = install

HTML_FILES = index.html cvs.html faq.html download.html \
	todo.html log.html doc.html list.html

web-site : $(HTML_FILES)
	@echo Web site build

%.html: %.m4 OFSETlib.m4 ASSETMLlib.m4 LAYOUTlib.m4
	m4 -dp -I$(M4LIB) -P < $< > $@

clean: 
	rm -f $(HTML_FILES)

install:
	$(INSTALL) -d $(DEST)
	$(INSTALL) *.html $(DEST)
	$(INSTALL) *.png $(DEST)
	$(INSTALL) *.jpg $(DEST)

